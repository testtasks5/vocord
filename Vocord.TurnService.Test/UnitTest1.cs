using Vocord.TurnService;
namespace Vocord.TurnService.Test;

public class UnitTest1
{
    [Theory]
    [InlineData("sss", "sss")]
    [InlineData("cat", "rat")]
    [InlineData("cats", "cat")]
    public void Test1(string s1, string s2)
    {
        var result = TurnService.TurnString(s1, s2);

        Assert.Equal(true, result);
    }
    [Theory]
    [InlineData("cats","rat")]
    [InlineData("aaa","abb")]
    [InlineData("ccc","bbbbbbbb")]
    public void Test2(string s1, string s2)
    {
        var result = TurnService.TurnString(s1, s2);

        Assert.Equal(false, result);
    }
}