

namespace Vocord.TurnService;

public class TurnService
{
    public static bool TurnString(string s1, string s2)
    {
        int count = 0;
        if(s1.Length - s2.Length > 1 || s1.Length - s2.Length < -1)
            return false;
        if(s1.Length != s2.Length)
            count++;
        int length = s1.Length > s2.Length ? s2.Length : s1.Length;
        for (int i = 0; i < length; i++)
        {
            if(s1[i] != s2[i] && count == 1)
                return false;
            else
                count++;
        }
        return true;
    }
}